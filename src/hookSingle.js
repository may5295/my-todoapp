import React, {useState} from 'react';
import './Appnew.css';
function HookSingle (props) {
    const [value, setValue] = useState(props.todo.currentValue);
    const [hide, setHide] = useState(true);
    const [ischecked] = useState(props.todo.ischecked);
    const checkbox = () => {
        props.check()
    }
    const handleChange = e => {
        setValue(e.target.value)
    }
    const toggle = () => {
        setHide(!hide)
    }

    const update = () => {
        props.update(props.ind,value)
        setHide(!hide)
    }
    return (
        <li>
            <input type='checkbox' onChange={checkbox} checked={ischecked}></input>
            {
                hide ?
                    props.todo.currentValue :
                    <input type='text' value={value} onChange={handleChange}></input>
            }
            <button onClick={props.delete}>Delete</button>
            {
                hide ?
                    <button onClick={toggle}>Edit</button> :
                    <button onClick={update}>save</button>
            }
        </li>
    );
}
export default HookSingle;