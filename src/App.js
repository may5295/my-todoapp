import React, { Component } from 'react';
import SingleTodo from "./SingleTodo";
import './Appnew.css';
class App extends Component {
  constructor() {
    super();
    this.state = {
      todos: [],
      currentTodo: { currentValue: '', ischecked: false, id: Math.random()}
    };
  }
  onInputChange = e => {
    this.setState({ currentTodo: { currentValue: e.target.value, ischecked: false, id: Math.random() } })
  }
  onClick = () => {
    let todosCopy = this.state.todos.slice();
    todosCopy.push(this.state.currentTodo);
    this.setState({ todos: todosCopy, currentTodo: { currentValue: '', ischecked: false, id: Math.random() } });
  }
  deleteTodo = id => () => {
    let todosCopy = [...this.state.todos]
    let result = todosCopy.findIndex((currentValue) => {
      return currentValue.id === id;
    })
    todosCopy.splice(result, 1);
    this.setState({ todos: [...todosCopy] });
  }
  update = (id, v) => {
    let clone = [...this.state.todos]
    let result = clone.find(currentTodo => currentTodo.id === id)
    result.currentValue = v
    this.setState({
      todos: clone
    })
  }

  checked = id => {
    let clone = [...this.state.todos]
    let result = clone.find(currentTodo => currentTodo.id === id)
    //result = {...result, ischecked: !result.ischecked}
    result.ischecked =!result.ischecked
    console.log(result)
    this.setState({
      todos: clone
    })
  }

  render() {
    console.log(this.state.todos)
    return (
      <div className='Appnew'>
        <div className='add'>
          <input placeholder="Enter todo" value={this.state.currentTodo.currentValue} onChange={this.onInputChange}></input>
          <button onClick={this.onClick}>Add!</button>
        </div>
        <div className='item'>
          <div className='Todos'>
            Todos List
            <br />
            {this.state.todos.length === 0 ?
              'No todos yet!' :
              <ul>
                {this.state.todos
                  .filter(x => x.ischecked === false)
                  .map((e, i) => {
                    return (
                      <SingleTodo ind={e.id} key={i} todo={e} update={this.update} delete={this.deleteTodo(e.id)} check={() => this.checked(e.id)} />
                    );
                  })
                }
              </ul>
            }
          </div> 
          <div className='done'>
            Done
            <br />
            {this.state.todos.length === 0 ?
              'No todos yet!' :
              <ul>
                {this.state.todos
                  .filter(x => x.ischecked === true)
                  .map((e, i) => {
                    return (
                      <SingleTodo ind={e.id} key={i} todo={e} update={this.update} delete={this.deleteTodo(e.id)} check={() => this.checked(e.id)} />
                    );
                  })
                }
              </ul>
            }
          </div>
        </div>
      </div>
    )
  }
}



export default App;
