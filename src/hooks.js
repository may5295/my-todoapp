import React, {useState} from 'react';
import HookSingle from "./hookSingle";
import './Appnew.css';
function Hooktodos () {
    const [todos, setTodos] = useState([]);
    const [currentTodo, setCurrentTodo] = useState({ currentValue: '', ischecked: false, id: Math.random()})
    const onInputChange = e => {
        setCurrentTodo({currentValue: e.target.value, ischecked: false, id: Math.random() })
      }
    const onClick = () => {
        let todosCopy = todos.slice();
        todosCopy.push(currentTodo);
        setTodos(todosCopy);
        setCurrentTodo({currentValue: '', ischecked: false, id: Math.random()})
      }
    const deleteTodo = id => () => {
        let todosCopy = [...todos]
        let result = todosCopy.findIndex((currentValue) => {
          return currentValue.id === id;
        })
        todosCopy.splice(result, 1);
        setTodos([...todosCopy]);
      }
    const update = (id, v) => {
        let clone = [...todos]
        let result = clone.find(currentTodo => currentTodo.id === id)
        result.currentValue = v
        setTodos(clone)
      }
    
    const checked = id => {
        let clone = [...todos]
        let result = clone.find(currentTodo => currentTodo.id === id)
        result.ischecked =!result.ischecked
        console.log(result)
        setTodos(clone)
      }
    return (
        <div className='Appnew'>
        <div className='add'>
          <input placeholder="Enter todo" value={currentTodo.currentValue} onChange={onInputChange}></input>
          <button onClick={onClick}>Add!</button>
        </div>
        <div className='item'>
          <div className='Todos'>
            Todos List
            <br />
            {todos.length === 0 ?
              'No todos yet!' :
              <ul>
                {todos
                  .filter(x => x.ischecked === false)
                  .map((e, i) => {
                    return (
                      <HookSingle ind={e.id} key={i} todo={e} update={update} delete={deleteTodo(e.id)} check={() => checked(e.id)} />
                    );
                  })
                }
              </ul>
            }
          </div> 
          <div className='done'>
            Done
            <br />
            {todos.length === 0 ?
              'No todos yet!' :
              <ul>
                {todos
                  .filter(x => x.ischecked === true)
                  .map((e, i) => {
                    return (
                      <HookSingle ind={e.id} key={i} todo={e} update={update} delete={deleteTodo(e.id)} check={() => checked(e.id)} />
                    );
                  })
                }
              </ul>
            }
          </div>
        </div>
      </div>
    )
}
export default Hooktodos;