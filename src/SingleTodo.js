import React, { Component } from "react";
class SingleTodo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.todo.currentValue,
            hide: true,
            ischecked: props.todo.ischecked
        }
    }
    checkbox = () => {
        this.props.check()
    }
    handleChange = e => {
        this.setState({
            value: e.target.value
        })
    }
    switch = () => {
        this.setState({
            hide: !this.state.hide,
            value: this.props.todo.currentValue
        })
    }



    update = () => {
        this.props.update(this.props.ind, this.state.value)
        this.setState({
            hide: !this.state.hide
        })
    }
    render() {
        return (
            <li>
                <input type='checkbox' onChange={this.checkbox} checked={this.state.ischecked}></input>
                {
                    this.state.hide ?
                        this.props.todo.currentValue :
                        <input type='text' value={this.state.value} onChange={this.handleChange}></input>
                }
                <button onClick={this.props.delete}>Delete</button>
                {
                    this.state.hide ?
                        <button onClick={this.switch}>Edit</button> :
                        <button onClick={this.update}>save</button>
                }
            </li>
        );
    }
}
export default SingleTodo;